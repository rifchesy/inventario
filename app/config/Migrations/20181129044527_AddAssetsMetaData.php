<?php
use Migrations\AbstractMigration;

class AddAssetsMetaData extends AbstractMigration
{

    public function up()
    {

        $this->table('assets')
            ->addColumn('manufacturer', 'string', [
                'after' => 'container_id',
                'default' => null,
                'length' => 255,
                'null' => true,
            ])
            ->addColumn('model', 'string', [
                'after' => 'manufacturer',
                'default' => null,
                'length' => 255,
                'null' => true,
            ])
            ->addColumn('serial', 'string', [
                'after' => 'model',
                'default' => null,
                'length' => 255,
                'null' => true,
            ])
            ->update();

        $this->table('manufacturers')->drop()->save();
    }

    public function down()
    {

        $this->table('manufacturers')
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('deleted', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('description', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('assets')
            ->removeColumn('manufacturer')
            ->removeColumn('model')
            ->removeColumn('serial')
            ->update();
    }
}

