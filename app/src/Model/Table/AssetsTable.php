<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Assets Model
 *
 * @method \App\Model\Entity\Asset get($primaryKey, $options = [])
 * @method \App\Model\Entity\Asset newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Asset[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Asset|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Asset|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Asset patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Asset[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Asset findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AssetsTable extends Table
{
    public $enums = array(
        'status' => array(
            'Ordered',
            'Owned',
            'Loaned Out',
            'Destroyed',
            'Lost',
            'Stolen',
            'Sold'
        )
    );

    public $actsAs = [
		'Search.Search',
		'Tags.Tag' => ['taggedCounter' => false]
    ];
    
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('assets');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Rooms', [
            'foreignKey' => 'room_id'
        ]);

        $this->belongsTo('Containers', [
            'foreignKey' => 'container_id'
        ]);

        $this->hasMany('Files', [
            'foreignKey' => 'asset_id'
        ]);

        $this->addBehavior('Enum');
        $this->addBehavior('Tags.Tag', ['taggedCounter' => false]);
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'photo' => [
                'fields' => [
                    'dir' => 'dir',
                    'size' => 'dir',
                    'type' => 'file_type',
                ],
            ],
        ]);

        $this->addBehavior('Search.Search');

        $this->addBehavior('Muffin/Trash.Trash');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->allowEmpty('title');

        $validator
            ->numeric('price_paid')
            ->allowEmpty('price_paid');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }

    public function addAsset($data) {
        $asset = $this->find('all',['conditions' => [
            'Assets.user_id' => $data['user_id'],
            'Assets.upc' => $data['upc']
        ]])->first();

        if (!$asset) {
            $asset = $this->newEntity();
        }

        $this->patchEntity($asset, ['tag_list' => $data['tags']]);
        unset($data['tags']);
        $this->patchEntity($asset, $data);

        $event = new Event('Model.Assets.beforeAdd', $this, [
            'asset' => $asset
        ]);
        $this->getEventManager()->dispatch($event);
        if (!empty($event->getResult()['asset'])) {
            $asset = $event->getResult()['asset'];
        }

        if (empty($asset->api_response) && !is_null($data['upc'])) {
            try {
                $response = file_get_contents("https://api.upcitemdb.com/prod/trial/lookup?upc=".$data['upc']);
                $lookupResult = json_decode($response);
                # First just store the API response
                $asset->api_response = $response;
            } catch (Exception $e) {
            }
        }

        $found_price_high = 0;
        # Should be sufficient.
        $found_price_low = 10000000000;
        $found_price_median = 0;
        if (isset($lookupResult)) {
            $prices = [];
            $product = $lookupResult->items[0];
            foreach($product->offers as $offer) {
                # Discard non USD currencies.
                if ($offer->currency == '') {
                    if ($offer->price > $found_price_high) {
                        $found_price_high = $offer->price;
                    }
                    if ($offer->price < $found_price_low) {
                        $found_price_low = $offer->price;
                    }
                    $prices[] = $offer->price;
                }
            }
            if (sizeof($prices) > 0) {
                rsort($prices);
                $middle = round(count($prices), 2);
                $found_price_median = $prices[$middle-1];
                $asset->found_price_high = $found_price_high;
                $asset->found_price_low = $found_price_low;
                $asset->found_price_median = $found_price_median;
            }
            $asset->upc = $data['title'];
            $asset->title = $product->title;
            if (sizeof($product->images) > 0 ) {
                $asset->image_url = $product->images[0];
            }
        }

        $asset->status = $this->enumValueToKey('status','Owned');

        if (isset($product->isbn)) {
            $asset->isbn = $product->isbn;
        }
        $this->save($asset);
        return $asset;
    }

    public function searchManager() {
        $searchManager = $this->behaviors()->Search->searchManager();
        $searchManager
            ->like('title', ['before' => true, 'after' => true])
            ->callback('tag', [
                'callback' => function (Query $query, array $args, $manager) {
                    if ($args['tag'] === '-1') {
                        $query->find('untagged');
                    } else {
                        $query->find('tagged', $args);
                    }
                    return true;
                }
            ]);
        return $searchManager;
    }

    # Try and parse and detect products from plain text imports of OCR'd scans of receipts
    public function import($text, $user_id) {
        $lines = explode(PHP_EOL,$text);
        $priceRegex = '@(\s?[0-9,]+([\. ]+[0-9]{2}))@i';
        foreach ($lines as $line) {
            $matches = array();
            $upc = null;
            preg_match_all("/([0-9]+)/",$line,$matches);
            foreach ($matches[0] as $number) {
                if (strlen($number) == 12) {
                    # We have a number that is probably a UPC.
                    $upc = $number;
                }
            }

            # Check for prices
            $matches = array();
            $price = null;
            preg_match($priceRegex, $line, $matches);
            if (isset($matches[0]) && is_numeric($matches[0])) {
                # We found what looks like a price.
                $price = $matches[0];
                # Remove the price from the line, then save it
                $line = preg_replace($priceRegex, '', $line);

                # Strip currency symbols
                # TODO: Add other currency symbols
                $line = preg_replace('/\$/', '', $line);
            }

            # Let's try to be a bit smart about this, and reject some obvious false positives
            if (
                strlen(trim($line)) < 4 ||
                # This could match things like Total cereal..
                // preg_match('/(total)/',$line) ||
                preg_match('/(Subtotal)/',$line)
            ) {
                continue;
            }

            # If we didn't find a price or upc, it's probably not an item.
            if ($price || $upc) {
                $this->addAsset([
                    'upc' => $upc,
                    'title' => trim($line),
                    'tags' => 'Imported',
                    'user_id' => $user_id,
                    'price_paid' => $price
                ]);
            }
        }
    }
}
