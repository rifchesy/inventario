<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Files Controller
 *
 * @property \App\Model\Table\FilesTable $Files
 *
 * @method \App\Model\Entity\File[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FilesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Assets', 'Containers', 'Rooms'],
            'conditions' => [
                'Files.user_id' => $this->request->getSession()->read('Auth.User.id')
            ]
        ];
        $files = $this->paginate($this->Files);

        $this->set(compact('files'));
    }

    /**
     * View method
     *
     * @param string|null $id File id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $file = $this->Files->get($id, [
            'contain' => ['Assets', 'Containers', 'Rooms']
        ]);

        $this->set('file', $file);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($assetId=null)
    {
        $file = $this->Files->newEntity();
        if ($this->request->is('post') || $this->request->is('put')) {
            $file = $this->Files->patchEntity($file, $this->request->getData());
            $file->user_id = $this->request->getSession()->read('Auth.User.id');
            if ($this->Files->save($file)) {
                $this->Flash->success(__('The file has been saved.'));
                if ($file->asset_id) {
                    return $this->redirect(['controller'=>'Assets','action' => 'view', $file->asset_id]);
                } else if ($file->container_id) {
                    return $this->redirect(['controller'=>'Containers','action' => 'view', $file->container_id]);
                } else if ($file->room_id) {
                    return $this->redirect(['controller'=>'Rooms','action' => 'view', $file->room_id]);
                }
            }
            $this->Flash->error(__('The file could not be saved. Please, try again.'));
        }
        $assets = $this->Files->Assets->find('list', ['limit' => 200]);
        $containers = $this->Files->Containers->find('list', ['limit' => 200]);
        $rooms = $this->Files->Rooms->find('list', ['limit' => 200]);
        $this->set(compact('file', 'assets', 'containers', 'rooms'));
    }

    /**
     * Edit method
     *
     * @param string|null $id File id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $file = $this->Files->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $file = $this->Files->patchEntity($file, $this->request->getData());
            if ($this->Files->save($file)) {
                $this->Flash->success(__('The file has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The file could not be saved. Please, try again.'));
        }
        $assets = $this->Files->Assets->find('list', ['limit' => 200]);
        $containers = $this->Files->Containers->find('list', ['limit' => 200]);
        $rooms = $this->Files->Rooms->find('list', ['limit' => 200]);
        $this->set(compact('file', 'assets', 'containers', 'rooms'));
    }

    /**
     * Delete method
     *
     * @param string|null $id File id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $file = $this->Files->get($id);
        if ($this->Files->delete($file)) {
            $this->Flash->success(__('The file has been deleted.'));
        } else {
            $this->Flash->error(__('The file could not be deleted. Please, try again.'));
        }
        if ($file->asset_id) {
            return $this->redirect(['controller'=>'Assets','action' => 'view',$file->asset_id]);
        } else if ($file->container_id) {
            return $this->redirect(['controller'=>'Containers','action' => 'view',$file->container_id]);
        } else if ($file->room_id) {
            return $this->redirect(['controller'=>'Rooms','action' => 'view',$file->room_id]);
        }
    }
}
