<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;

class CalculationHelper extends Helper
{

    public function getABV($og, $fg) {
      # https://www.brewersfriend.com/2011/06/16/alcohol-by-volume-calculator-updated/
      return round(($og - $fg) * 131.25,2);
    }

    public function getFermentCompletionPercentage($batch) {
      $today = new \DateTime(); 
      if ($batch->brew_date && $batch->package_date) {
        return round(($today->diff($batch->package_date)->d / $batch->brew_date->diff($batch->package_date)->d * 100));
      } else {
        return "0";
      }
    }

    public function getPintsPerGallons($gallons) {
      return round($gallons * 7.999975,1);
    }
}