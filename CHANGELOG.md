# v0.2 - Release Candidate

## Added
* Image uploads
* Filtering by tags

# v0.1 - Initial Release

## Added
* Easy asset/item importing with UPC support
* Rooms and containers
* Docker image